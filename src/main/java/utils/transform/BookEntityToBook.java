package utils.transform;

import entity.BookEntity;
import entity.BorrowedBookEntity;
import model.Book;
import utils.transform.abstraction.CollectionTransformer;

import java.util.Collection;
import java.util.List;

public class BookEntityToBook {

    public static List<Book> entityToBook(List<BookEntity> list, int userId) {
        CollectionTransformer bookTransformer = new CollectionTransformer<BookEntity, Book>() {
            @Override
            public Book transform(BookEntity bookEntity) {
                Integer borrowedUserId = null;
                Collection<BorrowedBookEntity> c = bookEntity.getBorrowedBooksByBookId();

                if (c.size() == 0) {
                } else {
                    for (BorrowedBookEntity b : c) {
                        if (b.getReturnDt() == null) {
                            if (b.getClientId() == userId) {
                                borrowedUserId = userId;
                                break;
                            } else {
                                return null;
                            }
                        }
                    }
                }

                Book book = new Book(
                        bookEntity.getBookId(),
                        bookEntity.getBookName(),
                        bookEntity.getIsbn(),
                        bookEntity.getBookshelf(),
                        bookEntity.getPhysicalSn(),
                        bookEntity.getPublishDt(),
                        borrowedUserId
                );

                return book;
            }
        };

        return bookTransformer.transform(list);
    }
}
