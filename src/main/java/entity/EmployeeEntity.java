package entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "employee", schema = "public", catalog = "db19_shcheden")
public class EmployeeEntity {
    private int employeeId;
    private String email;
    private String phoneNumber;
    private String position;
    private BigDecimal salary;
    private Integer managerId;
    private PersonEntity personByEmployeeId;
    private EmployeeEntity employeeByManagerId;
    private Collection<EmployeeEntity> employeesByEmployeeId;

    @Id
    @Column(name = "employee_id", nullable = false)
    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone_number", nullable = true, length = 50)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "position", nullable = false, length = 50)
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "salary", nullable = true, precision = 2)
    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Basic
    @Column(name = "manager_id", nullable = true)
    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return employeeId == that.employeeId &&
                Objects.equals(email, that.email) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(position, that.position) &&
                Objects.equals(salary, that.salary) &&
                Objects.equals(managerId, that.managerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, email, phoneNumber, position, salary, managerId);
    }

    @OneToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "person_id", nullable = false)
    public PersonEntity getPersonByEmployeeId() {
        return personByEmployeeId;
    }

    public void setPersonByEmployeeId(PersonEntity personByEmployeeId) {
        this.personByEmployeeId = personByEmployeeId;
    }

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
    public EmployeeEntity getEmployeeByManagerId() {
        return employeeByManagerId;
    }

    public void setEmployeeByManagerId(EmployeeEntity employeeByManagerId) {
        this.employeeByManagerId = employeeByManagerId;
    }

    @OneToMany(mappedBy = "employeeByManagerId")
    public Collection<EmployeeEntity> getEmployeesByEmployeeId() {
        return employeesByEmployeeId;
    }

    public void setEmployeesByEmployeeId(Collection<EmployeeEntity> employeesByEmployeeId) {
        this.employeesByEmployeeId = employeesByEmployeeId;
    }
}
