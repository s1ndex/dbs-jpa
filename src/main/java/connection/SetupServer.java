package connection;

import entity.BookEntity;
import entity.BorrowedBookEntity;
import entity.PersonEntity;
import model.Book;
import repository.BookRepository;
import repository.BorrowRepository;
import repository.PersonRepository;
import utils.transform.BookEntityToBook;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;

public class SetupServer {

    private static EntityManager EM =
            Persistence
                    .createEntityManagerFactory("NewPersistenceUnit")
                    .createEntityManager();

    private BookRepository bookRepository = new BookRepository(EM);
    private PersonRepository personRepository = new PersonRepository(EM);
    private BorrowRepository borrowRepository = new BorrowRepository(EM);

    private ServerSocket server;
    private Socket connectionToClient;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private List<Book> books;

    public void serverSetup() throws IOException {
        server = new ServerSocket(65100);
    }

    public void listen() throws IOException {
        connectionToClient = server.accept();
        System.out.println("SERVER: Client is connected to Server: " + connectionToClient.getInetAddress());
    }

    public void setupStreams() throws IOException {
        output = new ObjectOutputStream(connectionToClient.getOutputStream());
        input = new ObjectInputStream(connectionToClient.getInputStream());
        output.flush();
    }

    public void closeStreams() throws IOException {
        output.close();
        input.close();
    }

    public void disconnectClient() throws IOException {
        connectionToClient.close();
    }

    public void shutDownServer() throws IOException {
        server.close();
    }

    public static <R> R executeInTransaction(Supplier<R> runnable) {
        EntityTransaction transaction = EM.getTransaction();
        transaction.begin();

        R result = runnable.get();

        transaction.commit();

        return result;
    }

    public void job(String message) throws IOException {
        String[] preparedMessage = processMessage(message);

        switch (preparedMessage[0]) {
            case "ALL":
//                EM.getEntityManagerFactory().getCache().evictAll();

                books = BookEntityToBook.entityToBook(
                        executeInTransaction(() -> bookRepository.getAll()),
                        Integer.parseInt(preparedMessage[1])
                );
                output.writeObject(books);
                output.flush();
                break;

            case "LOGIN":
                boolean isCorrect = personRepository.isLoginDataCorrect(preparedMessage[1], preparedMessage[2]);
                System.out.println("SERVER: Client's login attempt: " + isCorrect);

                output.writeObject(isCorrect);
                output.flush();
                break;

            case "ID":
                int id = personRepository.getPersonIdByUsername(preparedMessage[1]);

                output.writeObject(id);
                output.flush();
                break;

            case "BORROW":
//                borrowRepository.borrowBook(Integer.parseInt(preparedMessage[1]), Integer.parseInt(preparedMessage[2]));
                BorrowedBookEntity bbe = new BorrowedBookEntity();

                try {
                    executeInTransaction(() -> {
                        bbe.setClientId(Integer.parseInt(preparedMessage[1]));
                        bbe.setBookId(Integer.parseInt(preparedMessage[2]));
                        bbe.setBorrowDt(Date.valueOf(LocalDate.now()));
                        bbe.setReturnDt(null);

                        borrowRepository.create(bbe);

                        return "";
                    });

                    EM.refresh(bbe);

                    System.out.println(
                            "SERVER: Client has borrowed the book "
                                    + preparedMessage[2]
                    );
                } catch (Exception e) {
                    System.out.println("SERVER: Error while borrowing");
                }

                break;

            case "RETURN":
                BorrowedBookEntity rbe = borrowRepository.getBorrowEntity(
                        Integer.parseInt(preparedMessage[1]),
                        Integer.parseInt(preparedMessage[2])
                );

                if (rbe != null) {
                    try {
                        executeInTransaction(() -> {
                            rbe.setReturnDt(Date.valueOf(LocalDate.now()));

                            borrowRepository.update(rbe);

                            return "";
                        });

                        EM.refresh(rbe);

                        System.out.println(
                                "SERVER: Client has returned the book "
                                        + preparedMessage[2]
                        );
                    } catch (Exception e) {
                        System.out.println("SERVER: Error while returning");
                    }
                } else {
                    System.out.println("SERVER: The book is already returned");
                }
                break;

//            case "GETA":
//                break;

//            case "GETB":
//
//                break;

            case "QUIT":
                System.out.println("SERVER: Disconnecting client: " + connectionToClient.getInetAddress());
                closeStreams();
                disconnectClient();

                break;
        }
    }

    public String[] processMessage(String message) {
        String[] messageArr = message.split(" ");

        return messageArr;
    }

    public void run() throws IOException {

//        String message = "";
        serverSetup();

        while (true) {
            System.out.println("SERVER: Listening..");
            listen();

            System.out.println("SERVER: Set Up streams");
            setupStreams();

            Thread thread = new Thread(new Runnable() {
                String message = "";

                @Override
                public void run() {
                    do {
                        try {
                            message = (String) input.readObject();
                            System.out.println("CLIENT: " + message);
                            System.out.println("SERVER: Processing request");
                            SetupServer.this.job(message);
                            continue;
                        } catch (ClassNotFoundException | IOException classNotFoundException) {
                        }
                    } while (!message.equals("QUIT"));
                }
            });
            thread.start();

            System.out.println("SERVER: Client was successfully disconnected\n");
        }
    }
}
