package repository;

import entity.BookEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;

public class BookRepository extends CRUDRepository<BookEntity> {

    public BookRepository(EntityManager transactionManager) {
        super(transactionManager, BookEntity.class);
    }

    public void query() {
        transactionManager.createQuery("QUERY", clazz).getResultList();
    }

}
